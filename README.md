# YT Channel Cloner

Creates a website that will allow you to clone your YouTube page and run an ad-free experience for your Patreon subscribers, for channel archiving, or in case you reckon your channel is about to get dunked.

## Features

- Script to pull your videos with stats, metadata, thumbnails and subtitles, including auto-generated subtitles.
- Gives you a ready-to-serve website
- Looks OK
- Uses simple HTML5 video plaback

## Disclaimer

This project contains no unauthorised copyrighted material. Use of this project is at your own risk. Neither Judd West, nor any of the contributing developers, are liable for the use or misuse of this project. We expect users to be the copyright holders of the material they are using with this project.

We are not affiliated with, nor endorse, the content made available through use of this software.

For potential fat-cat asshole litigators, have a look at [what happened to youtube-dl](https://www.eff.org/deeplinks/2020/11/github-reinstates-youtube-dl-after-riaas-abuse-dmca). See how well that went.

## Requirements

- Node 14.x or higher
- youtube-dl
- ffmpeg
- a computer that can run bash scripts. Should work on macos and Linux (TODO: Make Windows batch file)
- Your YouTube channel (no, not someone else's)

## How to use

1. Pull this repo or download the zip and extract onto your computer.
2. run `npm i` or `yarn` to install the dependencies
3. open terminal in `videos` folder, and run `./getytchannel.bash`. Don't run this from the root, it'll pull the videos into your $PWD
4. Enter the YouTube channel and optionally, the path to a cookies file (gets around some 18+ blocks and weirdness)
  - Note: YouTube don't like this. It's against ToS. I don't personally care, but they will push 403s and 404s to throw your downloads off. Just restart the process, and it'll continue where it left off.
5. create a file at the root of the project called `.env`. Inside, copy and edit the following:
  ```
    # port you're serving on
    WEB_PORT=3000 

    # name of the channel
    CHANNEL_NAME=MyYouTubeChannel

    # title for the website
    SITE_TITLE="MyYouTubeChannel Archive"

    # social links
    SOCIAL_YOUTUBE="https://www.youtube.com/user/somebody"
    SOCIAL_FACEBOOK="https://www.facebook.com/somebody"
    SOCIAL_TWITTER="https://twitter.com/somebody"
    SOCIAL_PODCAST="https://somebody.podbean.com/"
    SOCIAL_INSTAGRAM="https://www.instagram.com/somebody/"
    SOCIAL_DISCORD="https://discordapp.com/invite/someserver"
    SOCIAL_KOFI="https://ko-fi.com/somebody"
    SOCIAL_PATREON="https://www.patreon.com/somebody"
  ```
6. Add your YouTube banner to `public/banner.jpg`. Please use the same filename and a jpg. Something around the same size is recommended.
7. run `npm run start` or `yarn start` to begin serving the website.

## Contributing

Heya, I love it when people contribute. PRs are welcome.

## Issues

They go in the issues tracker

