require('dotenv').config()
const { app } = require('./src/routes')

const server = app.listen(process.env.WEB_PORT, () => {
    console.log(`The application started on port ${server.address().port}`);
});