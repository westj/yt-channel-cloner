const fs = require('fs');

const _getImages = (fileList, data) => {
    const splitfile = data._filename.split('.')
    splitfile.pop()
    const commonFilename = splitfile.join('.')

    return fileList.filter(a => a.includes(commonFilename)).filter(a => /\b(?:webp|jpg|jpeg|png)\b/.test(a))

}

const _getDescription = (fileList, data) => {
    const splitfile = data._filename.split('.')
    splitfile.pop()
    const commonFilename = splitfile.join('.')

    const descFile = fileList.filter(a => a.includes(commonFilename)).filter(a => /\b(?:description)\b/.test(a))[0]
    if (descFile && descFile.length) return fs.readFileSync(`./videos/${descFile}`, {encoding:'utf8', flag:'r'})
    return ''

}

const _getVideoFile = (fileList, data) => {
    const splitfile = data._filename.split('.')
    splitfile.pop()
    const commonFilename = splitfile.join('.')

    const videoFile = fileList.filter(a => a.includes(commonFilename)).filter(a => /\b(?:mp4|webm)\b/.test(a))[0]
    return videoFile

}

const _getVideoSubtitles = (fileList, data) => {
    const splitfile = data._filename.split('.')
    splitfile.pop()
    const commonFilename = splitfile.join('.')

    const subtitle = fileList.filter(a => a.includes(commonFilename)).filter(a => /\b(?:vtt)\b/.test(a))
    return subtitle ? subtitle[0] : {}

}

const collateVideos = () => {
    const jsonToScan = fs.readdirSync('./videos').filter(a => a.includes('json'))
    const res = jsonToScan.map(vid => {
        const data = JSON.parse(fs.readFileSync(`./videos/${vid}`))

        const {id, title, duration, upload_date, _filename} = data

        return {
            id,
            title,
            duration,
            date: upload_date,
            file:  _getVideoFile(fs.readdirSync('./videos'), {_filename}),
            thumbnail: _getImages(fs.readdirSync('./videos'), {_filename})[0],
            description: _getDescription(fs.readdirSync('./videos'), {_filename})
        }

    }).sort((a, b) => b.date - a.date)

    return res
}

const getVideo = (res, id) => {
    const dirlist = fs.readdirSync('./videos')
    const files = fs.readdirSync('./videos').filter(a => a.includes('json')).filter(a => a.includes(id))

    if (files.length === 0) res.status(404).send('404 Not Found')

    const result = files.map(vid => {
        const data = JSON.parse(fs.readFileSync(`./videos/${vid}`))

        const {id, title, duration, upload_date, _filename} = data

        return {
            id,
            title,
            duration,
            date: upload_date,
            file:  _getVideoFile(dirlist, {_filename}),
            thumbnail: _getImages(dirlist, {_filename})[0],
            description: _getDescription(dirlist, {_filename}),
            subtitles: _getVideoSubtitles(dirlist, {_filename})
        }

    })

    console.log(result)

    return result[0]

}

module.exports = {
    collateVideos,
    getVideo
}