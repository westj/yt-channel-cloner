const express = require('express');
const path = require('path');
const fs = require('fs');
const { collateVideos, getVideo } = require('./video')
var linkify = require('linkifyjs');
var linkifyHtml = require('linkifyjs/html');

const app = express();

const buttons = [
    {
        link: process.env.SOCIAL_YOUTUBE,
        icon: 'fab fa-youtube'
    },
    {
        link: process.env.SOCIAL_FACEBOOK,
        icon: 'fab fa-facebook'
    },
    {
        link: process.env.SOCIAL_TWITTER,
        icon: 'fab fa-twitter'
    },
    {
        link: process.env.SOCIAL_PODCAST,
        icon: 'fas fa-podcast'
    },
    {
        link: process.env.SOCIAL_INSTAGRAM,
        icon: 'fab fa-instagram'
    },
    {
        link: process.env.SOCIAL_DISCORD,
        icon: 'fab fa-discord'
    },
    {
        link: process.env.SOCIAL_KOFI,
        icon: 'fas fa-donate'
    },
    {
        link: process.env.SOCIAL_PATREON,
        icon: 'fab fa-patreon'
    }
]

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static('public'))

// Strip useless position data from subtitles. Fixes left-aligned subtitles
app.use('*.vtt', (req, res) => {
    console.log(req.params[0])
    const subtitles = fs.readFileSync('.' + req.params[0] + '.vtt',  {encoding:'utf8', flag:'r'})
    res.send(subtitles.split('align:start position:0%').join(''))
})

app.use('/videos', express.static('videos'))

app.get('/', (req, res) => {
    res.render('home', {req, videos: collateVideos(), vars: process.env, buttons});
})

app.get('/video/:id', (req, res) => {
    res.render('video', {req, video: getVideo(res, req.params.id), vars: process.env, buttons, linkifyHtml});
})

module.exports = {
    app,
}