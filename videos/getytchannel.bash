#!/bin/bash

read -p "Please enter a YouTube Channel URL: " ytc
read -p "Please enter path to a YouTube cookies file (optional): " cookies

cookie=$([ "$cookies" != "" ] && echo "--cookies $cookies" || echo '')

youtube-dl --write-description --write-info --write-thumbnail --write-sub --write-auto-sub --write-annotations -f 'bestvideo[ext=webm]+bestaudio[ext=m4a]/best[ext=mp4]/best' --merge-output-format mp4 $cookie $ytc
